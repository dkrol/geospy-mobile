﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using System.Threading;
using GeoSpyMobile.Helpers;
using System.Text;
using System.Collections.Generic;
using Newtonsoft.Json;
using GeoSpyMobile.Models;
using GeoSpyMobile.BLL;
using GeoSpyMobile.Config;
using Android.Content;
using GeoSpyMobile;
using Android.Telephony;
using static GeoSpyMobile.Helpers.AlertHelper;

namespace GeoSpyMobile
{
    [Activity(Label = "GeoSpy", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {
        private DeviceManager deviceManager;
        private LoginManager loginManager;
        private SignupManager signupManager;

        private Button btnSignin;
        private Button btnSignup;
        private ProgressBar progressBar;
        private TextView txtSigninValid;
        private EditText txtUserName;
        private EditText txtPassword;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);

            deviceManager = new DeviceManager(this);
            loginManager = new LoginManager();
            signupManager = new SignupManager();

            initViewReferences();
            initEvents();

            //string url = "http://192.168.0.101:9810/api/Users";
            //HttpHelper httpHelper = new HttpHelper(url);

            //httpHelper.WebClient.DownloadDataAsync(httpHelper.Url);
            //httpHelper.WebClient.DownloadDataCompleted += WebClient_DownloadDataCompleted;

        }

        private void initViewReferences()
        {
            progressBar = FindViewById<ProgressBar>(Resource.Id.progressBar);
            btnSignup = FindViewById<Button>(Resource.Id.btnSignup);
            btnSignin = FindViewById<Button>(Resource.Id.btnSignin);
            txtSigninValid = FindViewById<TextView>(Resource.Id.txtSigninValid);
            txtUserName = FindViewById<EditText>(Resource.Id.txtUserName);
            txtPassword = FindViewById<EditText>(Resource.Id.txtPassword);
        }

        private void initEvents()
        {
            btnSignup.Click += BtnSignup_Click;
            btnSignin.Click += BtnSignin_Click;
        }

        private void BtnSignin_Click(object sender, EventArgs e)
        {
            IUserLogin user = new User();

            user.UserName = txtUserName.Text;
            user.Password = txtPassword.Text;

            //user.UserName = "d@d.pl";
            //user.Password = "Qwerty12?";

            bool result = loginManager.Signin(user as IUserLogin);
            if (result)
            {
                deviceManager.AddDevice(user);
                Intent intent = new Intent(this, typeof(SplashActivity));
                this.StartActivity(intent);
                this.Finish();
            }
            else
            {
                txtSigninValid.Text = "Incorrect login or password";
            }
        }

        //private void WebClient_DownloadDataCompleted(object sender, System.Net.DownloadDataCompletedEventArgs e)
        //{
        //    List<userTest> userList;
        //    List<userTest2> userList2;
        //    RunOnUiThread(() =>
        //    {
        //        string json = Encoding.UTF8.GetString(e.Result);
        //        userList = JsonConvert.DeserializeObject<List<userTest>>(json);

        //        userList2 = JsonConvert.DeserializeObject<List<userTest2>>(json);

        //    });

        //}

        private void BtnSignup_Click(object sender, System.EventArgs e)
        {
            FragmentTransaction transaction = FragmentManager.BeginTransaction();
            DialogSignupActivity dialogSingup = new DialogSignupActivity();
            dialogSingup.Show(transaction, "DialogSignupLayout");
            dialogSingup.onSignupComplete += DialogSingup_onSignupComplete;
        }

        private void DialogSingup_onSignupComplete(object sender, OnSignupEventArgs e)
        {
            progressBar.Visibility = Android.Views.ViewStates.Visible;
            Thread thread = new Thread(ActionRequest);
            thread.Start();
            IUserSignup user = e.User;
            bool result = signupManager.SignupUser(user);
            string msg = result ? "Registration successfull" : "Registration ERROR";
            Toast.MakeText(this, msg, ToastLength.Short).Show();
        }

        private void ActionRequest()
        {
            Thread.Sleep(3000);
            RunOnUiThread(() => { progressBar.Visibility = Android.Views.ViewStates.Invisible; });
        }
    }
}

