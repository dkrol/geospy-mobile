using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using GeoSpyMobile.Models;
using GeoSpyMobile;

namespace GeoSpyMobile
{
    class DialogSignupData : Activity
    {
        private View view;
        public EditText TxtEmail { get; set; }
        public EditText TxtPassword { get; set; }
        public EditText TxtConfirmPassword { get; set; }
        public Button BtnSignup { get; set; }
        public TextView TxtEmailValid { get; set; }
        public TextView TxtPasswordValid { get; set; }
        public TextView TxtConfirmPasswordValid { get; set; }

        public DialogSignupData(View view)
        {
            this.view = view;
            TxtEmail = view.FindViewById<EditText>(Resource.Id.txtEmailDialog);
            TxtPassword = view.FindViewById<EditText>(Resource.Id.txtPasswordDialog);
            TxtConfirmPassword = view.FindViewById<EditText>(Resource.Id.txtConfirmPasswordDialog);
            BtnSignup = view.FindViewById<Button>(Resource.Id.btnSignupDialog);
            TxtEmailValid = view.FindViewById<TextView>(Resource.Id.txtEmailDialogValid);
            TxtPasswordValid = view.FindViewById<TextView>(Resource.Id.txtPasswordDialogValid);
            TxtConfirmPasswordValid = view.FindViewById<TextView>(Resource.Id.txtConfirmPasswordDialogValid);

            SetHints();
        }

        private void SetHints()
        {
            TxtEmail.SetHint(Resource.String.txtEmail);
            TxtPassword.SetHint(Resource.String.txtPassword);
            TxtConfirmPassword.SetHint(Resource.String.txtConfirmPassword);
        }
    }

    class OnSignupEventArgs : EventArgs
    {
        private IUserSignup user;
        public IUserSignup User
        {
            get { return user; }
            set { user = value; }
        }
        public OnSignupEventArgs(DialogSignupData data) : base()
        {
            User = new User();
            User.Email = data.TxtEmail.Text;
            User.Password = data.TxtPassword.Text;
            User.ConfirmPassword = data.TxtConfirmPassword.Text;
        }
    } 

    class DialogSignupActivity : DialogFragment
    {
        private DialogSignupData data;
        public event EventHandler<OnSignupEventArgs> onSignupComplete;
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.DialogSingupLayout, container, false);
            data = new DialogSignupData(view);
            data.BtnSignup.Click += BtnSignup_Click; 
            return view;
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
        }

        private void BtnSignup_Click(object sender, EventArgs e)
        {
            bool isValid = ValidationData(data);
            if (isValid)
            {
                onSignupComplete.Invoke(this, new OnSignupEventArgs(data));
                this.Dismiss();
            }
        }

        private bool ValidationData(DialogSignupData data)
        {
            bool result = true;
            if (string.IsNullOrEmpty(data.TxtEmail.Text))
            {
                data.TxtEmailValid.Text = "Required field";
                result = false;
            }
            else
            {
                data.TxtEmailValid.Text = string.Empty;
            }

            if (string.IsNullOrEmpty(data.TxtPassword.Text))
            {
                data.TxtPasswordValid.Text = "Required field";
                result = false;
            }
            else
            {
                data.TxtPasswordValid.Text = string.Empty;
            }

            if (string.IsNullOrEmpty(data.TxtConfirmPassword.Text))
            {
                data.TxtConfirmPasswordValid.Text = "Required field";
                result = false;
            }
            else
            {
                data.TxtConfirmPasswordValid.Text = string.Empty;
            }

            return result;
        }
    }
}