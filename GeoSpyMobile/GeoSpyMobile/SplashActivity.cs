using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using GeoSpyMobile.Helpers;
using GeoSpyMobile.Config;
using Newtonsoft.Json;
using GeoSpyMobile.Models;

namespace GeoSpyMobile
{
    [Activity(Label = "GeoSpy", MainLauncher = true, Icon = "@drawable/icon")]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Intent intent;
            string tokenJson = SharedPref.GetString(AppConfig.USER_CONFIG, AppConfig.TOKEN);
            if (tokenJson == string.Empty)
            {
                SharedPref.Clear(AppConfig.USER_CONFIG);
                intent = new Intent(this, typeof(MainActivity));

            }
            else
            {
                Token token = JsonConvert.DeserializeObject<Token>(tokenJson);
                token.Json = tokenJson;
                // TODO: Check expiriens date token
                intent = new Intent(this, typeof(DashboardActivity));
            }

            this.StartActivity(intent);
            this.Finish();
        }
    }
}