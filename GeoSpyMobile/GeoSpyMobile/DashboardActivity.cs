using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Locations;
using Android.Util;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Android.Content.PM;
using Plugin.Permissions;
using GeoSpyMobile;
using GeoSpyMobile.Helpers;
using GeoSpyMobile.Config;
using GeoSpyMobile.Services;
using Plugin.Geolocator.Abstractions;
using GeoSpyMobile.Models;
using Newtonsoft.Json;

namespace GeoSpyMobile
{
    [Activity(Label = "Dashboard", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class DashboardActivity : Activity
    {
        public static Activity CurrentActivity;
        private Button btnLogout;
        private Intent locationService;
        private TextView txtDeviceInfo;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.DashboardLayout);
            CurrentActivity = this;

            InitView();
            InitService();
            InitData();
        }

        private void InitView()
        {
            btnLogout = FindViewById<Button>(Resource.Id.btnLogout);
            btnLogout.Click += BtnLogout_Click;
            txtDeviceInfo = FindViewById<TextView>(Resource.Id.txtDeviceInfo);
        }

        private void InitService()
        {
            locationService = new Intent(this, typeof(LocationService));
            StartService(locationService);
        }

        private void InitData()
        {
            string deviceJson = SharedPref.GetString(AppConfig.DEVICE_CONFIG, AppConfig.DEVICE);
            string tokenJson = SharedPref.GetString(AppConfig.USER_CONFIG, AppConfig.TOKEN);
            Device device = JsonConvert.DeserializeObject<Device>(deviceJson);
            Token token = JsonConvert.DeserializeObject<Token>(tokenJson);

            txtDeviceInfo.Text = string.Empty;
            txtDeviceInfo.Text += string.Format("User: \r\r\r\r\r{0}\n", token.userName);
            txtDeviceInfo.Text += string.Format("Device: \r{0}\n", device.Name);
            txtDeviceInfo.Text += string.Format("IMEI: \r\r\r\r\r{0}\n", device.Imei);
        }

        private void BtnLogout_Click(object sender, EventArgs e)
        {
            StopService(locationService);
            SharedPref.Clear(AppConfig.USER_CONFIG);
            Intent intent = new Intent(this, typeof(SplashActivity));
            this.StartActivity(intent);
            this.Finish();
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public static void UpdateView(double latitude, double longitude, double speed)
        {
            TextView txtView = CurrentActivity.FindViewById<TextView>(Resource.Id.txtLocation);
            txtView.Text = string.Format("Latitude: \r\r\r{0}\nLongitude: \r{1}\nSpeed: \r{2}", latitude, longitude, speed);
        }
    }


    [BroadcastReceiver(Enabled = true)]
    [IntentFilter(new[] { "geospy.location.changed" })]
    public class LocationReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            if (intent.Action == "geospy.location.changed")
            { 
                Position pos = new Position();
                double latitude = intent.GetDoubleExtra("Latitude", 0);
                double longitude = intent.GetDoubleExtra("Longitude", 0);
                double speed = intent.GetDoubleExtra("Speed", 0);
                DashboardActivity.UpdateView(latitude, longitude, speed);
            }
        }
    }
}