using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Accounts;

namespace GeoSpyMobile.Config
{
    public class AppConfig
    {
        //public const string API_URL = "http://192.168.137.1:9810";
        //public const string API_URL = "http://192.168.0.101:9810";
        public const string API_URL = "http://192.168.137.1:28424";

        public const string SIGNIN_URL = API_URL + "/Token";
        public const string USERS_URL = API_URL + "/api/users";
        public const string SIGNUP_URL = API_URL + "/api/Account/Register";
        public const string DEVICE_URL = API_URL + "/api/devices";
        public const string LOCATION_URL = API_URL + "/api/locations";

        public const string USER_CONFIG = "USER_CONFIG";
        public const string TOKEN = "TOKEN";

        public const string DEVICE_CONFIG = "DEVICE_CONFIG";
        public const string DEVICE = "DEVICE_INFO";


    }
}