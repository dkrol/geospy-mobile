using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;

namespace GeoSpyMobile.Helpers
{
    public class HttpHelper
    {
        private WebClient webClient;
        private Uri url;

        public event DownloadDataCompletedEventHandler DownloadDataCompleted
        {
            add
            {
                webClient.DownloadDataCompleted += value;
            }
            remove
            {
                webClient.DownloadDataCompleted -= value;
            }
        }

        public HttpHelper(string url)
        {
            this.webClient = new WebClient();
            this.url = new Uri(url);
        }

        public void DownloadDataAsync()
        {
            webClient.DownloadDataAsync(url);
        }

        public void DownloadDataAsync(DownloadDataCompletedEventHandler downloadDataCompleted)
        {
            DownloadDataAsync();
            webClient.DownloadDataCompleted += downloadDataCompleted;
        }
    }
}