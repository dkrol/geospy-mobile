using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Collections.Specialized;
using System.Net;
using GeoSpyMobile.Models;
using GeoSpyMobile.Config;
using Newtonsoft.Json;

namespace GeoSpyMobile.Helpers
{
    public static class Http
    {
        public static HttpResponse Get(string uri, bool setToken = false)
        {
            HttpResponse httpResponse = new HttpResponse();
            using (WebClient client = new WebClient())
            {
                try
                {
                    if (setToken)
                    {
                        Token token = GetToken();
                        string bearerToken = "Bearer " + token.access_token;
                        client.Headers.Add("Authorization", bearerToken);
                    }
                    httpResponse.Response = client.DownloadData(uri);
                    httpResponse.Status = true;
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    httpResponse.Status = false;
                }
                
            }
            return httpResponse;
        }

        public static HttpResponse Post(string uri, object obj, bool setToken = false)
        {
            HttpResponse httpResponse = new HttpResponse();
            NameValueCollection data = GetNameValueCollection(obj);
            using (WebClient client = new WebClient())
            {
                try
                {
                    if (setToken)
                    {
                        Token token = GetToken();
                        string bearerToken = "Bearer " + token.access_token;
                        client.Headers.Add("Authorization", bearerToken);
                    }
                    httpResponse.Response = client.UploadValues(uri, data);
                    httpResponse.Status = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    httpResponse.Status = false;
                }
            }
            return httpResponse;
        }

        public static HttpResponse Signin(string uri, object obj)
        {
            HttpResponse httpResponse = new HttpResponse();
            NameValueCollection data = GetNameValueCollection(obj);
            using (WebClient client = new WebClient())
            {
                try
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    httpResponse.Response = client.UploadValues(uri, data);
                    httpResponse.Status = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    httpResponse.Status = false;
                }
            }
            return httpResponse;
        }

        private static Token GetToken()
        {
            string tokenJson = SharedPref.GetString(AppConfig.USER_CONFIG, AppConfig.TOKEN);
            Token token = JsonConvert.DeserializeObject<Token>(tokenJson);
            return token;
        }

        private static NameValueCollection GetNameValueCollection(object obj)
        {
            NameValueCollection formFields = new NameValueCollection();
            var formFieldsProp = obj.GetType().GetProperties().ToList();
            foreach (var prop in formFieldsProp)
            {
                var value = prop.GetValue(obj, null);
                if (value != null)
                {
                    formFields.Add(prop.Name, value.ToString());
                }
            }
            return formFields;
        }
    }
}