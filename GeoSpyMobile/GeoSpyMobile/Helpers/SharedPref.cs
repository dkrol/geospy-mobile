using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GeoSpyMobile.Helpers
{
    public static class SharedPref
    {
        private static ISharedPreferences sharedPref;
        private static ISharedPreferencesEditor sharedEdit;
        public static void SetSharedPrefPrivate(string objName, Dictionary<string, string> dict) 
        {
            sharedPref = Application.Context.GetSharedPreferences(objName, FileCreationMode.Private);
        }

        public static void PutString(string objName, string key, string value)
        {
            sharedPref = Application.Context.GetSharedPreferences(objName, FileCreationMode.Private);
            sharedEdit = sharedPref.Edit();
            sharedEdit.PutString(key, value);
            sharedEdit.Apply();
        }

        public static void PutStringSet(string objName, string key, List<string> values)
        {
            sharedPref = Application.Context.GetSharedPreferences(objName, FileCreationMode.Private);
            sharedEdit = sharedPref.Edit();
            sharedEdit.PutStringSet(key, values);
            sharedEdit.Apply();
        }

        public static string GetString(string objName, string key)
        {
            sharedPref = Application.Context.GetSharedPreferences(objName, FileCreationMode.Private);
            string result = sharedPref.GetString(key, string.Empty);
            return result;
        }

        public static ICollection<string> GetStringSet(string objName, string key)
        {
            ICollection<string> values = new List<string>();
            sharedPref = Application.Context.GetSharedPreferences(objName, FileCreationMode.Private);
            values = sharedPref.GetStringSet(key, values);
            return values;
        }

        public static void Clear(string objName)
        {
            sharedPref = Application.Context.GetSharedPreferences(objName, FileCreationMode.Private);
            sharedEdit = sharedPref.Edit();
            sharedEdit.Clear();
            sharedEdit.Apply();
        }
    }
}