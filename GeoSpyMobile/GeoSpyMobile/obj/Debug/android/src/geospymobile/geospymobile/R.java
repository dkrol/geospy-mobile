/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package geospymobile.geospymobile;

public final class R {
    public static final class attr {
    }
    public static final class drawable {
        public static final int icon=0x7f020000;
    }
    public static final class id {
        public static final int btnLogout=0x7f050005;
        public static final int btnSignin=0x7f050012;
        public static final int btnSignup=0x7f050013;
        public static final int btnSignupDialog=0x7f05000d;
        public static final int progressBar=0x7f050015;
        public static final int relativeLayout1=0x7f050014;
        public static final int textView1=0x7f050000;
        public static final int textView2=0x7f050001;
        public static final int textView3=0x7f050003;
        public static final int txtConfirmPasswordDialog=0x7f05000b;
        public static final int txtConfirmPasswordDialogValid=0x7f05000c;
        public static final int txtDeviceInfo=0x7f050002;
        public static final int txtEmailDialog=0x7f050007;
        public static final int txtEmailDialogValid=0x7f050008;
        public static final int txtGeoSpy=0x7f05000e;
        public static final int txtLocation=0x7f050004;
        public static final int txtPassword=0x7f050010;
        public static final int txtPasswordDialog=0x7f050009;
        public static final int txtPasswordDialogValid=0x7f05000a;
        public static final int txtRegistrationDialog=0x7f050006;
        public static final int txtSigninValid=0x7f050011;
        public static final int txtUserName=0x7f05000f;
    }
    public static final class layout {
        public static final int dashboardlayout=0x7f030000;
        public static final int dialogsinguplayout=0x7f030001;
        public static final int main=0x7f030002;
    }
    public static final class string {
        public static final int ApplicationName=0x7f040001;
        public static final int btnSignin=0x7f040000;
        public static final int btnSignup=0x7f040004;
        public static final int titleLogging=0x7f040002;
        public static final int txtConfirmPassword=0x7f040007;
        public static final int txtEmail=0x7f040005;
        public static final int txtOr=0x7f040003;
        public static final int txtPassword=0x7f040006;
    }
}
