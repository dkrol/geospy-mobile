package md5135b28b2fbbc7a981f152330356ee6ad;


public class DialogSignupData
	extends android.app.Activity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("Activities.GeoSpyMobile.DialogSignupData, GeoSpyMobile, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", DialogSignupData.class, __md_methods);
	}


	public DialogSignupData () throws java.lang.Throwable
	{
		super ();
		if (getClass () == DialogSignupData.class)
			mono.android.TypeManager.Activate ("Activities.GeoSpyMobile.DialogSignupData, GeoSpyMobile, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public DialogSignupData (android.view.View p0) throws java.lang.Throwable
	{
		super ();
		if (getClass () == DialogSignupData.class)
			mono.android.TypeManager.Activate ("Activities.GeoSpyMobile.DialogSignupData, GeoSpyMobile, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Views.View, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
