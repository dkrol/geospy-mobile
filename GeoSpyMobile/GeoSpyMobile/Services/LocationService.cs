using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using GeoSpyMobile.Models;
using GeoSpyMobile.Helpers;
using GeoSpyMobile.Config;
using Newtonsoft.Json;
using GeoSpyMobile.BLL;
using System.Globalization;

namespace GeoSpyMobile.Services
{
    [Service(Exported = true, Name = "geospy.LocationService")]
    public class LocationService : Service
    {
        private int minTime = 1000;
        private double minDistance = 1;
        private double desiredAccuracy = 5;
        private Device device;
        private LocationManager locationManager;
        private DeviceManager deviceManager;
        private IGeolocator locator;
        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        public override void OnCreate()
        {
            base.OnCreate();
            locationManager = new LocationManager();
            deviceManager = new DeviceManager();
            device = deviceManager.GetDeviceFromSharedPref();
            InitLocation();
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            locator.StopListeningAsync();
        }

        private async void InitLocation()
        {
            try
            {
                locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = desiredAccuracy;
                locator.AllowsBackgroundUpdates = true;
                locator.PositionChanged += Locator_PositionChanged;
                locator.PositionError += Locator_PositionError;
                await locator.StartListeningAsync(minTime, minDistance, true);
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, "No localization", ToastLength.Short).Show();
                Console.WriteLine(ex.Message);
            }
        }

        private void Locator_PositionError(object sender, PositionErrorEventArgs e)
        {
            Console.WriteLine("Locator_PositionError!");
        }

        private void Locator_PositionChanged(object sender, PositionEventArgs e)
        {
            Position position = e.Position;
            Location location = new Location
            {
                Latitude = position.Latitude,
                Longitude = position.Longitude,
                Timestamp = position.Timestamp.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss").Replace('.','/'),
                Speed = position.Speed,
                DeviceId = device.Id
            };

            locationManager.AddLocation(location);
            Console.WriteLine("Position Status: {0}", position.Timestamp);
            Console.WriteLine("Position Latitude: {0}", position.Latitude);
            Console.WriteLine("Position Longitude: {0}", position.Longitude);

            var intent = new Intent("geospy.location.changed");
            intent.PutExtra("Latitude", position.Latitude);
            intent.PutExtra("Longitude", position.Latitude);
            intent.PutExtra("Speed", position.Speed);
            SendBroadcast(intent);
        }
    }
}