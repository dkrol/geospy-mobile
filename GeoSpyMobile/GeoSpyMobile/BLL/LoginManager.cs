using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using GeoSpyMobile.Models;
using GeoSpyMobile.Helpers;
using GeoSpyMobile.Config;

namespace GeoSpyMobile.BLL
{
    public class LoginManager
    {
        public bool Signin(IUserLogin user)
        {
            HttpResponse response = Http.Signin(AppConfig.SIGNIN_URL, user);
            if (response.Status)
            {
                SharedPref.PutString(AppConfig.USER_CONFIG, AppConfig.TOKEN, response.ResponseJson);
            }
            return response.Status;
        }
    }
}