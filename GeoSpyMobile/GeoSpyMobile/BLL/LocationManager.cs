using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using GeoSpyMobile.Models;
using GeoSpyMobile.Config;
using GeoSpyMobile.Helpers;

namespace GeoSpyMobile.BLL
{
    class LocationManager
    {
        public void AddLocation(Location location)
        {
            HttpResponse response = Http.Post(AppConfig.LOCATION_URL, location, setToken: true);
        }
    }
}