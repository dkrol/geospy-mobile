using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using GeoSpyMobile.Models;
using GeoSpyMobile.Helpers;
using GeoSpyMobile.Config;

namespace GeoSpyMobile.BLL
{
    public class SignupManager
    {
        public bool SignupUser(IUserSignup user)
        {
            HttpResponse response = Http.Post(AppConfig.SIGNUP_URL, user); 
            return response.Status;
        }
    }
}