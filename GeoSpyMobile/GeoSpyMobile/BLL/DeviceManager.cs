using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using GeoSpyMobile.Models;
using Android.Telephony;
using GeoSpyMobile.Helpers;
using GeoSpyMobile.Config;
using Newtonsoft.Json;

namespace GeoSpyMobile.BLL
{
    public class DeviceManager
    {
        private readonly Activity activity;
        public DeviceManager(Activity activity)
        {
            this.activity = activity;
        }

        public DeviceManager() : this(null) { }

        public void AddDevice(IUserLogin user)
        {
            Device device = GetCurrentDevice();
            device.UserName = user.UserName;
            HttpResponse response = Http.Post(AppConfig.DEVICE_URL, device, setToken: true);
            if (response.Status)
            {
                SharedPref.PutString(AppConfig.DEVICE_CONFIG, AppConfig.DEVICE, response.ResponseJson); 
            }
        }

        public Device GetCurrentDevice()
        {
            Device device = null;
            if (activity != null)
            {
                device = new Device();
                TelephonyManager telephonyManager = (TelephonyManager)activity.ApplicationContext.GetSystemService(Context.TelephonyService);
                device.Imei = telephonyManager.DeviceId;
                device.Name = telephonyManager.MmsUserAgent;
            }
            return device;
        }

        public List<Device> GetDevices()
        {
            HttpResponse response = Http.Get(AppConfig.DEVICE_URL, setToken: true);
            List<Device> devices = JsonConvert.DeserializeObject<List<Device>>(response.ResponseJson);
            return devices;
        }

        public Device GetDeviceByImei(string imei)
        {
            // Http get device by imei

            return new Device();
        }

        public Device GetDeviceFromSharedPref()
        {
            string deviceJson = SharedPref.GetString(AppConfig.DEVICE_CONFIG, AppConfig.DEVICE);
            Device device = JsonConvert.DeserializeObject<Device>(deviceJson);
            return device;
        }

        // function AddDevice()
        // function GetDeviceByUserId()
        // function GetDebiceById()

    }
}