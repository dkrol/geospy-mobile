using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GeoSpyMobile.Models
{
    public class User : IUserSignup, IUserLogin
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public string Grant_type
        {
            get
            {
                return "password";
            }
        }
        public string UserName { get; set; }
    }
}