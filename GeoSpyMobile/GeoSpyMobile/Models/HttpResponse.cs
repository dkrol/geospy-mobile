using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GeoSpyMobile.Models
{
    public class HttpResponse
    {
        public byte[] Response { get; set; }
        public bool Status { get; set; }
        public string ResponseJson
        {
            get
            {
                string result = string.Empty;
                if (Response != null)
                {
                    result = Encoding.UTF8.GetString(Response);
                }
                return result;
            }
        }
    }
}