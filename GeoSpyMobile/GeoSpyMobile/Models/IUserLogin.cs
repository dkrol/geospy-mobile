using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GeoSpyMobile.Models
{
    public interface IUserLogin
    {
        string Grant_type { get; }
        string UserName { get; set; }
        string Password { get; set; }
    }
}